#!/usr/bin/python
import os
from flask import Flask, render_template,make_response
import hashlib
from urllib.request import urlopen, Request
from urllib.error import URLError
baseurl="http://sandbox.thesubdb.com/"
myAgent="SubDB/1.0 (StutzelSubs/0.1; http://gitlab.com/matheusstutzel/gerenciamento-legendas)"

#http://pt.thesubdb.com/api/
def get_hash(name):
    readsize = 64 * 1024
    with open(name, 'rb') as f:
        size = os.path.getsize(name)
        data = f.read(readsize)
        f.seek(-readsize, os.SEEK_END)
        data += f.read(readsize)
    return hashlib.md5(data).hexdigest()

def make_tree(path,lvl=0,maxlvl=3,includeFiles=True):
    if (lvl==maxlvl):
        return 0
    tree = dict(name=os.path.basename(path), children=[],url=path)
    try: lst = os.listdir(path)
    except OSError:
        pass #ignore errors
    else:
        for name in lst:
            if (name.startswith(".")):
                continue
            fn = os.path.join(path, name)
            if (lvl+1<maxlvl and os.path.isdir(fn)):
                tree['children'].append(make_tree(fn,lvl+1,maxlvl,includeFiles))
            else:
                #tree['children'].append(dict(name=name))
                if (name.startswith("poster")):
                    tree["img"]=fn
                if (includeFiles and name[-3:] in ["mkv","avi","mp4"]):
                    tree['children'].append(dict(name=name,url="/@@"+fn))
    return tree

app = Flask(__name__)
@app.route('/')
def defaultRoute():
    tree = dict(name="Sala", children=[])
    tree['children'].append(make_tree("/media/hdseries/Series",2,includeFiles=False))
    tree['children'].append(make_tree("/media/hd10002/hd1000/Filmes",2,includeFiles=False))
    tree['children'].append(make_tree("/media/hd10003/Filmes",2,includeFiles=False))
    return render_template('dirtree.html', tree=tree)

@app.route('/@@/<path:path>')
def legenda(path):
    path="/"+path
    lg="pt"
    srt=subDB_download(get_hash(path),lg)    
    if(not srt):
        lg="en"
        srt=srt=subDB_download(get_hash(path),lg)    
    if(srt):
        with open(path[:-3]+lg+".srt",'w') as f:
            f.write(str(srt))
            return "Salvando legenda em "+lg+" com o nome:"+path[:-3]+lg+".srt"
        
    else:
        return "Não encontrei nenhuma legenda para "+path

def subDB_download(hash,language):
    try:
        url=baseurl+"?action=download&hash="+hash+"&language="+language
        req = Request(
            url, 
            data=None, 
            headers={
                'User-Agent': myAgent
            }
        )
        f = urlopen(req)
        return f.read().decode()
    except URLError:
        return False


@app.route('/<path:path>/Series')
def showcase_series(path):
    return render_template('dirtree.html', tree=make_tree("/"+path+"/Series",maxlvl=2,includeFiles=False))

@app.route('/<path:path>/Filmes')
def showcase_filmes(path):
    return render_template('dirtree.html', tree=make_tree("/"+path+"/Filmes",maxlvl=2,includeFiles=True))

@app.route('/<path:path>')
def dirtree(path):
    return render_template('dirtree.html', tree=make_tree("/"+path))

@app.route('/media/<path:path>.jpg')
def show_post(path):
    with open("/media/"+path+".jpg", 'rb') as f:
        resp = make_response(f.read())
        resp.headers['Content-type'] = 'image/jpg'
        return resp


if __name__=="__main__":
    app.run(host='0.0.0.0', port=8888, debug=True)


